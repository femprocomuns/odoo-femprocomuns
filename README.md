# femprocomuns Odoo module 

This is by now a Proof of Concept (PoC) of a custom Odoo module for femprocums, aimed on looking for a proper way of installing custom Odoo modules and its dependencies in the most automated way possible.

Actual module logic is not being implemented yet.

The PoC objectives are:

* Ensure this module can be installed, resolving its dependencies, in an Odoo 10 instance.
* Dependencies are Odoo core modules:
  * Project
  * Membership.

# Requirements

- An Odoo 10 instance.
- Pip

# Pre-requisites

- You have an Odoo working instance, and an Odoo settings file somewhere (usually in `opt/odoo/shared/odoo.conf`). We are going to call that file path `ODOO_CONFIG_FILE`.

- In Odoo configuration, you should have set an `addons_path` setting. Be sure you install this module on one of the paths defined on that setting. We are going to call that path `ODOO_ADDONS_PATH`.

- Also, you should set Odoo database name. We are going to call that `ODOO_DATABASE_NAME`. 

# Installation instructions

Installation requires only one manual step, that implies accesing Odoo web interface. It's needed in order to make Odoo walk through the modules directory. I haven't found an automatic way of doing this yet.

1. Go to your `ODOO_ADDONS_PATH` and grab `poc` branch:

```
$ wget https://gitlab.com/femprocomuns/odoo-femprocomuns/repository/poc/archive.zip
```

1. Unzip:

```
$ unzip archive.zip -d femprocomuns
```

1. Go to Odoo web interface and activate developer mode in settings. Now go to Apps > Update Apps List.

1. Install module:

```
$ python /opt/odoo/current/build/scripts-2.7/odoo -c<ODOO_CONFIG_FILE> --init=femprocomuns -d <ODOO_DATABASE_NAME>
```

For instance:

```
$ /opt/.odoo_venv/bin/python /opt/odoo/current/build/scripts-2.7/odoo -copt/odoo/shared/odoo.conf --init=femprocomuns -d odoo
```

1. That's it, you should be able to enter Odoo web site, look for the femprocomuns app and see that it is marked as installed.
